// SPDX-License-Identifier: MIT

//go:generate go run ./make_config.go
//go:generate go run ../../cmd/apidoc/main.go ../../docs/example

package docs
