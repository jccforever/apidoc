module github.com/caixw/apidoc/v5

require (
	github.com/issue9/assert v1.3.4
	github.com/issue9/is v1.3.2
	github.com/issue9/term v1.1.0
	github.com/issue9/utils v1.1.1
	github.com/issue9/version v1.0.3
	golang.org/x/text v0.3.2
	gopkg.in/yaml.v2 v2.2.2
)

go 1.13
